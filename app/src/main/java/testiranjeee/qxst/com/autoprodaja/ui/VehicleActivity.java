package testiranjeee.qxst.com.autoprodaja.ui;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.List;

import testiranjeee.qxst.com.autoprodaja.R;
import testiranjeee.qxst.com.autoprodaja.adapter.CustomApplicationClass;
import testiranjeee.qxst.com.autoprodaja.model.Vehicle;
import testiranjeee.qxst.com.autoprodaja.model.VehicleDetailed;
import testiranjeee.qxst.com.autoprodaja.model.Vehicles;

public class VehicleActivity extends Fragment {

    private static final String POSITION = "position";
    private int position = -1;
    private FragmentActivity fragmentActivity;
    private ImageView detailsImage;
    private ImageView invertedImage;
    private Vehicles vehiclesDetails;
    private TextView yeartw;
    private TextView pricetw;
    private TextView marktw;
    private TextView modeltw;
    private TextView updatedtw;
    private LinearLayout rootLinear;
    private int vehicleid;
    private ScrollView scrollview;
    private boolean loaded;
    private ProgressBar progressBar;
    private LinearLayout rootExtra;
    private CustomApplicationClass appClass;

    static VehicleActivity newInstance(int num) {
        VehicleActivity f = new VehicleActivity();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentActivity = super.getActivity();
        scrollview = (ScrollView) inflater.inflate(R.layout.activity_vehicle, container, false);
        loaded = false;
        rootExtra = (LinearLayout) scrollview.findViewById(R.id.extraDetails);
        progressBar = new ProgressBar(getActivity());
        position = getArguments() != null ? getArguments().getInt("num") : 1;
        appClass = (CustomApplicationClass) fragmentActivity.getApplicationContext();
        detailsImage = (ImageView) scrollview.findViewById(R.id.detailsPageImg);
        rootLinear = (LinearLayout) scrollview.findViewById(R.id.detailsRoot);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        rootLinear.getLayoutParams().height = ((size.y) / 10) * 9;
        rootLinear.getLayoutParams().width = size.x;
        rootLinear.requestLayout();
        invertedImage = (ImageView) scrollview.findViewById(R.id.detailsPageImgRv);
        yeartw = (TextView) scrollview.findViewById(R.id.detailsYear);
        pricetw = (TextView) scrollview.findViewById(R.id.detailsPrice);
        marktw = (TextView) scrollview.findViewById(R.id.detailsMark);
        modeltw = (TextView) scrollview.findViewById(R.id.detailsModel);
        updatedtw = (TextView) scrollview.findViewById(R.id.detailsUpdated);
        initializeDetails(position);

        scrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scrollview.getScrollY() > 15 && !loaded) {
                    loaded = true;
                    progressBar.setEnabled(true);
                    progressBar.setVisibility(View.VISIBLE);
                    rootExtra.addView(progressBar);
                    getDetailedData(vehicleid);
                }
            }
        });
        return scrollview;

    }

    //  @Override
   /* protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);



    }*/
    //  public boolean onOptionsItemSelected(MenuItem item) {
    //      onBackPressed();

    //      return true;
    //  }

    public void initializeDetails(int position) {
        Vehicle vehicleDetails = getVehicle(position);
        String url = vehicleDetails.getImage();
        String year = ("Godina proizvodnje: " + vehicleDetails.getYear());
        String price = ("Cena: " + vehicleDetails.getPrice());
        String mark = ("Proizvodjac: " + vehicleDetails.getMarkName());
        String model = ("Model: " + vehicleDetails.getModelName());
        String updated = ("Poslednja izmena: " + vehicleDetails.getUpdatedAt());

        Glide.with(this).load(url).into(detailsImage);
        Glide.with(this).load(url).into(invertedImage);
        yeartw.setText(year);
        pricetw.setText(price);
        marktw.setText(mark);
        modeltw.setText(model);
        updatedtw.setText(updated);
        vehicleid = vehicleDetails.getId();
    }

    public Vehicle getVehicle(int position) {
        vehiclesDetails = appClass.getVehiclesInstance();
        List<Vehicle> vehicleListDetails = vehiclesDetails.getVehicles();
        Vehicle vehicleDetails = vehicleListDetails.get(position);
        return vehicleDetails;
    }
    //  public void onBackPressed(){
    //      super.onBackPressed();
    //  }

    private void getDetailedData(int id) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = ("http://quadrixsoft.com/auto/public/api/vehicles/" + id);

        JsonObjectRequest jsobjreq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject response) {

                String output = response.toString();
                Gson gson = new GsonBuilder().create();
                VehicleDetailed vehicleDetailed = gson.fromJson(output, VehicleDetailed.class);
                fillDetails(vehicleDetailed);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity().getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        queue.add(jsobjreq);
    }

    private void fillDetails(VehicleDetailed vehicleDetailed) {
        TextView year = new TextView(getActivity());
        year.setPadding(4, 4, 4, 4);
        //  text.setTextColor(getResources().getColor(R.color.textColorPrimary));
        year.setText("Godina proizvodnje: " + vehicleDetailed.getYear());
        rootExtra.addView(year);
        TextView cc = new TextView(getActivity());
        cc.setPadding(4, 2, 2, 4);
        cc.setText("Zapremina motora : " + vehicleDetailed.getCc());
        rootExtra.addView(cc);
        TextView fuel = new TextView(getActivity());
        fuel.setText("Gorivo: " + vehicleDetailed.getFuel());
        fuel.setPadding(4, 2, 2, 4);
        rootExtra.addView(fuel);
        TextView power_kw = new TextView(getActivity());
        power_kw.setText("Snaga u kW: " + vehicleDetailed.getPowerKw());
        power_kw.setPadding(4, 2, 2, 4);
        rootExtra.addView(power_kw);
        TextView power_ks = new TextView(getActivity());
        power_ks.setText("Snaga u HP: " + vehicleDetailed.getPowerKs());
        power_ks.setPadding(4, 2, 2, 4);
        rootExtra.addView(power_ks);
        TextView kilometers = new TextView(getActivity());
        kilometers.setText("Kilometraza: " + vehicleDetailed.getKilometers());
        kilometers.setPadding(4, 2, 2, 4);
        rootExtra.addView(kilometers);
        TextView emission_class = new TextView(getActivity());
        emission_class.setText("Emisiona klasa: " + vehicleDetailed.getEmissionClass());
        emission_class.setPadding(4, 2, 2, 4);
        rootExtra.addView(emission_class);
        TextView drive = new TextView(getActivity());
        drive.setText("Pogon: " + vehicleDetailed.getDrive());
        drive.setPadding(4, 2, 2, 4);
        rootExtra.addView(drive);
        TextView gearshift = new TextView(getActivity());
        gearshift.setText("Menjac: " + vehicleDetailed.getGearshift());
        gearshift.setPadding(4, 2, 2, 4);
        rootExtra.addView(gearshift);
        TextView doors = new TextView(getActivity());
        doors.setText("Broj vrata: " + vehicleDetailed.getDoors());
        doors.setPadding(4, 2, 2, 4);
        rootExtra.addView(doors);
        TextView seats = new TextView(getActivity());
        seats.setText("Broj sedista: " + vehicleDetailed.getSeats());
        seats.setPadding(4, 2, 2, 4);
        rootExtra.addView(seats);
        TextView steering_wheel_side = new TextView(getActivity());
        steering_wheel_side.setText("Pozicija volana: " + vehicleDetailed.getSteeringWheelSide());
        steering_wheel_side.setPadding(4, 2, 2, 4);
        rootExtra.addView(steering_wheel_side);
        TextView air_conditioner = new TextView(getActivity());
        air_conditioner.setText("Klima uredjaj: " + vehicleDetailed.getAirConditioner());
        air_conditioner.setPadding(4, 2, 2, 4);
        rootExtra.addView(air_conditioner);
        TextView color = new TextView(getActivity());
        color.setText("Boja: " + vehicleDetailed.getColor());
        color.setPadding(4, 2, 2, 4);
        rootExtra.addView(color);
        TextView interior_materials = new TextView(getActivity());
        interior_materials.setText("Enterijer: " + vehicleDetailed.getInteriorMaterials());
        interior_materials.setPadding(4, 2, 2, 4);
        rootExtra.addView(interior_materials);
        TextView interior_color = new TextView(getActivity());
        interior_color.setText("Boja enterijera: " + vehicleDetailed.getInteriorColor());
        interior_color.setPadding(4, 2, 2, 4);
        rootExtra.addView(interior_color);
        TextView registration = new TextView(getActivity());
        registration.setText("Registrovan do: " + vehicleDetailed.getRegistration());
        registration.setPadding(4, 2, 2, 4);
        rootExtra.addView(registration);
        TextView damage = new TextView(getActivity());
        damage.setText("Ostecenja: " + vehicleDetailed.getDamage());
        damage.setPadding(4, 2, 2, 4);
        rootExtra.addView(damage);
        TextView change = new TextView(getActivity());
        change.setText("Zamena: " + vehicleDetailed.getChange());
        change.setPadding(4, 2, 2, 4);
        rootExtra.addView(change);
        TextView origin = new TextView(getActivity());
        origin.setText("Tablice: " + vehicleDetailed.getOrigin());
        origin.setPadding(4, 2, 2, 4);
        rootExtra.addView(origin);
        TextView count = new TextView(getActivity());
        count.setText("Na stanju: " + vehicleDetailed.getCount());
        count.setPadding(4, 2, 2, 4);
        rootExtra.addView(count);
        TextView status = new TextView(getActivity());
        status.setText("Status: " + vehicleDetailed.getStatus());
        status.setPadding(4, 2, 2, 4);
        rootExtra.addView(status);
        TextView description = new TextView(getActivity());
        description.setText("Opis: " + vehicleDetailed.getDescription());
        description.setPadding(4, 2, 2, 4);
        rootExtra.addView(description);
        TextView created_at = new TextView(getActivity());
        created_at.setText("Oglas postavljen: " + vehicleDetailed.getCreatedAt());
        created_at.setPadding(4, 2, 2, 4);
        rootExtra.addView(created_at);
        TextView updated_at = new TextView(getActivity());
        updated_at.setText("Zadnji put izmenjen: " + vehicleDetailed.getUpdatedAt());
        updated_at.setPadding(4, 2, 2, 4);
        rootExtra.addView(updated_at);
        progressBar.setVisibility(View.GONE);


    }
}
