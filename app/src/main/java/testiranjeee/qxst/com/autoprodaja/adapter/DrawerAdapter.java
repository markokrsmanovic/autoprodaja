package testiranjeee.qxst.com.autoprodaja.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import testiranjeee.qxst.com.autoprodaja.R;
import testiranjeee.qxst.com.autoprodaja.model.DrawerItem;

/**
 * Created by Marko on 3/23/2017.
 */

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {


    private List<DrawerItem> drawerData;
    private final Context context;



    public DrawerAdapter(Context c, List<DrawerItem> drawerdata){
        super(c, -1, drawerdata);


        this.context = c;
        this.drawerData = drawerdata;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.drawer_layout, parent, false);
        TextView textview = (TextView) rowView.findViewById(R.id.drawer_text);
        ImageView imageview = (ImageView) rowView.findViewById(R.id.drawer_icon);
        DrawerItem item = drawerData.get(position);
        textview.setText(item.getDrawerOptions());
        imageview.setImageResource(item.getDrawerIcons());
        return rowView;




    }


}
