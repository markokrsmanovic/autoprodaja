package testiranjeee.qxst.com.autoprodaja.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import testiranjeee.qxst.com.autoprodaja.R;
import testiranjeee.qxst.com.autoprodaja.adapter.CustomApplicationClass;
import testiranjeee.qxst.com.autoprodaja.adapter.DrawerAdapter;
import testiranjeee.qxst.com.autoprodaja.adapter.LinearLayoutManagerWithSmoothScroller;
import testiranjeee.qxst.com.autoprodaja.adapter.RecyclerItemClickListener;
import testiranjeee.qxst.com.autoprodaja.adapter.VehicleAdapter;
import testiranjeee.qxst.com.autoprodaja.model.DrawerData;
import testiranjeee.qxst.com.autoprodaja.model.Vehicles;

public class CarsListActivity extends AppCompatActivity {

    private RecyclerView recView;
    private VehicleAdapter adapter;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private Vehicles vehiclesInstance;
    private CustomApplicationClass customApp;
    private static final String POSITION = "position";
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars_list);
        customApp = ((CustomApplicationClass)getApplicationContext());

        vehiclesInstance = new Vehicles();
        this.populate();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                populate();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new DrawerAdapter(this, DrawerData.getDrawerData()));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened
                getSupportActionBar().setTitle("Meni");
              //  getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable());
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()  
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
                getSupportActionBar().setTitle("AutoProdaja");
              //  getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_drawer);
                invalidateOptionsMenu();
            }


        };



        mDrawerLayout.addDrawerListener(mDrawerToggle);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_drawer);





    }

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }



   public void onResume(){
        super.onResume();
       if (vehiclesInstance == null) {
        setContentView(R.layout.activity_cars_list);
        customApp = ((CustomApplicationClass)getApplicationContext());
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

           this.initializeList();


       mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                populate();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new DrawerAdapter(this, DrawerData.getDrawerData()));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

       }
    }


    protected void populate() {
        RequestQueue queue = Volley.newRequestQueue(CarsListActivity.this);
        String url = ("http://quadrixsoft.com/auto/public/api/vehicles");

        JsonObjectRequest jsobjreq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject response) {

                String output = response.toString();
                Gson gson = new GsonBuilder().create();
                Vehicles vehicles = gson.fromJson(output, Vehicles.class);
                customApp.setVehiclesInstance(vehicles);
                initializeList();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        queue.add(jsobjreq);
    }

    public void initializeList() {
        recView = (RecyclerView) findViewById(R.id.rec_list);
        recView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(this));
        vehiclesInstance = customApp.getVehiclesInstance();
        adapter = new VehicleAdapter(vehiclesInstance.getVehicles(), this);
        adapter.notifyDataSetChanged();
        recView.setAdapter(adapter);
        recView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Intent intent = new Intent(CarsListActivity.this, CarDetailsViewPager.class);
                        intent.putExtra(POSITION, position);
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }
    private void selectItem(int position){
        if (position == 0){
            recView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(this));
        }
        else
            if (position == 1)
        {

            recView.setLayoutManager(new GridLayoutManager(this, 2));
        }
        mDrawerList.setItemChecked(position,true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }



}
