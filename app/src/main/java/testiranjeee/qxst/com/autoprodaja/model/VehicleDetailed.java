package testiranjeee.qxst.com.autoprodaja.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleDetailed {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("mark_id")
    @Expose
    private String markId;
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("vehicle_body_id")
    @Expose
    private String vehicleBodyId;
    @SerializedName("cc")
    @Expose
    private String cc;
    @SerializedName("fuel")
    @Expose
    private String fuel;
    @SerializedName("power_kw")
    @Expose
    private String powerKw;
    @SerializedName("power_ks")
    @Expose
    private String powerKs;
    @SerializedName("kilometers")
    @Expose
    private String kilometers;
    @SerializedName("emission_class")
    @Expose
    private String emissionClass;
    @SerializedName("drive")
    @Expose
    private String drive;
    @SerializedName("gearshift")
    @Expose
    private String gearshift;
    @SerializedName("doors")
    @Expose
    private String doors;
    @SerializedName("seats")
    @Expose
    private String seats;
    @SerializedName("steering_wheel_side")
    @Expose
    private String steeringWheelSide;
    @SerializedName("air_conditioner")
    @Expose
    private String airConditioner;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("interior_materials")
    @Expose
    private String interiorMaterials;
    @SerializedName("interior_color")
    @Expose
    private String interiorColor;
    @SerializedName("registration")
    @Expose
    private String registration;
    @SerializedName("damage")
    @Expose
    private String damage;
    @SerializedName("change")
    @Expose
    private String change;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("count")
    @Expose
    private String count;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("special_status")
    @Expose
    private Object specialStatus;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getMarkId() {
        return markId;
    }

    public void setMarkId(String markId) {
        this.markId = markId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVehicleBodyId() {
        return vehicleBodyId;
    }

    public void setVehicleBodyId(String vehicleBodyId) {
        this.vehicleBodyId = vehicleBodyId;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getPowerKw() {
        return powerKw;
    }

    public void setPowerKw(String powerKw) {
        this.powerKw = powerKw;
    }

    public String getPowerKs() {
        return powerKs;
    }

    public void setPowerKs(String powerKs) {
        this.powerKs = powerKs;
    }

    public String getKilometers() {
        return kilometers;
    }

    public void setKilometers(String kilometers) {
        this.kilometers = kilometers;
    }

    public String getEmissionClass() {
        return emissionClass;
    }

    public void setEmissionClass(String emissionClass) {
        this.emissionClass = emissionClass;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public String getGearshift() {
        return gearshift;
    }

    public void setGearshift(String gearshift) {
        this.gearshift = gearshift;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getSteeringWheelSide() {
        return steeringWheelSide;
    }

    public void setSteeringWheelSide(String steeringWheelSide) {
        this.steeringWheelSide = steeringWheelSide;
    }

    public String getAirConditioner() {
        return airConditioner;
    }

    public void setAirConditioner(String airConditioner) {
        this.airConditioner = airConditioner;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getInteriorMaterials() {
        return interiorMaterials;
    }

    public void setInteriorMaterials(String interiorMaterials) {
        this.interiorMaterials = interiorMaterials;
    }

    public String getInteriorColor() {
        return interiorColor;
    }

    public void setInteriorColor(String interiorColor) {
        this.interiorColor = interiorColor;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getSpecialStatus() {
        return specialStatus;
    }

    public void setSpecialStatus(Object specialStatus) {
        this.specialStatus = specialStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}