package testiranjeee.qxst.com.autoprodaja.model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Marko on 3/23/2017.
 */

public class Vehicles {
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("vehicles")
    @Expose
    private List<Vehicle> vehicles = new ArrayList<>();

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

}
