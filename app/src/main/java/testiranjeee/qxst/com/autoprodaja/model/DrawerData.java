package testiranjeee.qxst.com.autoprodaja.model;

import java.util.ArrayList;
import java.util.List;

import testiranjeee.qxst.com.autoprodaja.R;

/**
 * Created by Marko on 3/23/2017.
 */

public class DrawerData {
    private static final int[] drawerChoices = {R.string.vehicles, R.string.rethreading};
    private static final int[] drawerIcons = {R.drawable.motor, R.drawable.protektura};

    public static List<DrawerItem> getDrawerData(){
        List<DrawerItem> data = new ArrayList<>();
        {
            for (int i = 0; i < drawerChoices.length; i++) {
                DrawerItem item = new DrawerItem();
                item.setDrawerOptions(drawerChoices[i]);
                item.setDrawerIcons(drawerIcons[i]);
                data.add(item);
            }
        }
        return data;
    }
}
