package testiranjeee.qxst.com.autoprodaja.adapter;

import android.app.Application;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import testiranjeee.qxst.com.autoprodaja.model.Vehicles;

/**
 * Created by Marko on 3/25/2017.
 */

public class CustomApplicationClass extends Application {

    public Vehicles getVehiclesInstance() {
        return vehiclesInstance;
    }

    public void setVehiclesInstance(Vehicles vehiclesInstance) {
        this.vehiclesInstance = vehiclesInstance;
    }

    private Vehicles vehiclesInstance;

    public void onCreate(){
        super.onCreate();

    }


}
