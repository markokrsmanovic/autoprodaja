package testiranjeee.qxst.com.autoprodaja.model;

/**
 * Created by Marko on 3/23/2017.
 */

public class DrawerItem {
    private int drawerOptions;

    public int getDrawerOptions() {
        return drawerOptions;
    }

    public void setDrawerOptions(int drawerOptions) {
        this.drawerOptions = drawerOptions;
    }

    public int getDrawerIcons() {
        return drawerIcons;
    }

    public void setDrawerIcons(int drawerIcons) {
        this.drawerIcons = drawerIcons;
    }

    private int drawerIcons;
}
