package testiranjeee.qxst.com.autoprodaja.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import testiranjeee.qxst.com.autoprodaja.R;
import testiranjeee.qxst.com.autoprodaja.model.Vehicle;

/**
 * Created by Marko on 3/22/2017.
 */

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleHolder>{


    private List<Vehicle> vehicleData;
    private LayoutInflater inflater;
    private Context context;
    private int lastPosition = -1;

    public VehicleAdapter (List<Vehicle> vehicleData, Context c){
        this.inflater = LayoutInflater.from(c);
        this.vehicleData = vehicleData;
        this.context = c;
    }

    @Override
    public VehicleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.car_item, parent, false);
        return new VehicleHolder(view);
    }

    @Override
    public void onBindViewHolder(VehicleHolder holder, int position) {
        Vehicle item = vehicleData.get(position);
        holder.date.setText(item.getYear());
        holder.description.setText(item.getMarkName() + " " + item.getModelName());
        String url = item.getImage();
        Glide.with(this.context).load(url).into(holder.picture);
        setAnimation(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return vehicleData.size();
    }

    class VehicleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView date;
        private ImageView picture;
        private TextView description;
        private View container;


        public VehicleHolder(View itemView) {
            super(itemView);

            date = (TextView) itemView.findViewById(R.id.date);
            picture = (ImageView) itemView.findViewById(R.id.picture);
            description = (TextView) itemView.findViewById(R.id.description);
            container = itemView.findViewById(R.id.car_item_root);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION){

            }
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }

    }



}
